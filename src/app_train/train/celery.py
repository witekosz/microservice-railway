from celery import Celery

from train import settings


app = Celery("tasks", broker=settings.DATABASE_URI, include=["app_central.tasks"],)
