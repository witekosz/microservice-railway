def test_train_get_current_speed(train):
    """Check train current speed"""

    assert train.min_speed <= train.get_current_speed() <= train.max_speed


def test_train_get_nearest_station(train):
    """Check train get current station"""

    assert train.get_nearest_station() in train.stations_list
