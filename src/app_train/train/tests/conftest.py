import pytest

from .. import settings
from ..train import Train


@pytest.fixture
def train():
    """Create new train instance"""
    train: Train = Train(
        settings.TRAIN_MIN_SPEED,
        settings.TRAIN_MAX_SPEED,
        settings.TRAIN_STATIONS,
        settings.TRAIN_CURRENT_SPEED_INTERVAL,
        settings.TRAIN_NEAREST_STATION_INTERVAL,
    )

    return train
