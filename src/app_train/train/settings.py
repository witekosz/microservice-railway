import logging
import os


# Settings for train worker

# Train speed in km/h
TRAIN_MIN_SPEED = 0
TRAIN_MAX_SPEED = 180

# Train tasks interval in seconds
TRAIN_CURRENT_SPEED_INTERVAL = 10
TRAIN_NEAREST_STATION_INTERVAL = 180

# Train stations list from LK 7 https://pl.wikipedia.org/wiki/Linia_kolejowa_nr_7
TRAIN_STATIONS = [
    "Warszawa Zachodnia",
    "Warszawa Ochota",
    "Warszawa Śródmieście",
    "Warszawa Powiśle",
    "Warszawa Stadion",
    "Warszawa Wschodnia",
    "Warszawa Olszynka Grochowska",
    "Warszawa Wawer",
    "Warszawa Falenica",
    "Józefów",
    "Otwock",
    "Pogorzel Warszawska",
    "Celestynów",
    "Zabieżki",
    "Pilawa",
    "Garwolin",
    "Łaskarzew Przystanek",
    "Sobolew",
    "Życzyn",
    "Dęblin",
]

# Logging
LOGGER_FORMAT = "%(asctime)s %(message)s"

logging.basicConfig(format=LOGGER_FORMAT, datefmt="[%H:%M:%S]")
logger = logging.getLogger()
logger.setLevel(logging.INFO)

DATABASE_URI = os.environ.get("DATABASE_URI")
