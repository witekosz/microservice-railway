import asyncio
import random
from attr import dataclass
from typing import List

from train.settings import logger
from train.celery import app


@dataclass
class Train:
    """Class for train object"""

    min_speed: int
    max_speed: int
    stations_list: List[str]
    speed_interval: int
    stations_interval: int

    def get_current_speed(self) -> int:
        """Return random current speed"""
        return random.randint(self.min_speed, self.max_speed)

    def get_nearest_station(self) -> str:
        """Returns random name of station"""
        return random.choice(self.stations_list)

    async def check_current_speed(self) -> None:
        """Send task to celery worker"""

        while True:
            speed: int = self.get_current_speed()
            logger.info(speed)
            app.send_task(
                "tasks.actual_speed", args=(speed,),
            )

            await asyncio.sleep(self.speed_interval)

    async def check_nearest_station(self) -> None:
        """Send task to celery worker"""

        while True:
            station_name: str = self.get_nearest_station()
            logger.info(station_name)

            app.send_task(
                "tasks.train_station", args=(station_name,),
            )
            await asyncio.sleep(self.stations_interval)

    def start_train(self) -> None:
        """Simulate train move, runs asyncio loop to run tasks"""
        logger.info("Starting train")

        train_loop = asyncio.get_event_loop()

        # Asyncio tasks
        train_loop.create_task(self.check_current_speed())
        train_loop.create_task(self.check_nearest_station())

        train_loop.run_forever()
