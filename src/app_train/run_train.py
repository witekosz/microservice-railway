from train import settings
from train.train import Train


train: Train = Train(
    settings.TRAIN_MIN_SPEED,
    settings.TRAIN_MAX_SPEED,
    settings.TRAIN_STATIONS,
    settings.TRAIN_CURRENT_SPEED_INTERVAL,
    settings.TRAIN_NEAREST_STATION_INTERVAL,
)


if __name__ == "__main__":
    train.start_train()
