from datetime import datetime

import requests

import settings


def check_if_barrier_status_is_open() -> bool:
    """Send get request to api service and returns True if barrier status is open"""

    response = requests.get(settings.API_URL, verify=False)

    if response.status_code == 200:
        status = response.json()["status"]

        if status == "opened":
            return True
        else:
            return False


def change_barrier_status(status: bool) -> dict:
    """Send post request to api service and closes or opens barrier status"""

    status_message: str
    if status:
        status_message = "opened"
    else:
        status_message = "closed"

    form_data: dict = {"barrier_status": status_message}
    response = requests.post(settings.API_URL, data=form_data)
    now = datetime.now().strftime("[%H:%M:%S]")

    if response.status_code == 200:
        return {"time": now, "status": response.json()["status"]}


def write_log(file_name: str, message: str) -> None:
    with open(settings.LOG_DIR / file_name, "a") as file:
        file.write(message)
