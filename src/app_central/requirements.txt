kombu
kombu-sqlalchemy
celery==4.3.0
requests
psycopg2==2.8.4

pytest
coverage
pytest-sugar
pytest-xdist
