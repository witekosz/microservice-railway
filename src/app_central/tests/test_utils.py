import os

import settings
from utils import check_if_barrier_status_is_open, change_barrier_status, write_log


def test_check_if_barrier_status_is_open():

    response = check_if_barrier_status_is_open()

    assert isinstance(response, bool)


def test_change_barrier_status_close():

    response = change_barrier_status(False)

    assert response["status"] == "closed"


def test_change_barrier_status_open():

    response = change_barrier_status(True)

    assert response["status"] == "opened"


def test_write_log():
    write_log("test.log", "Test message")

    with open(settings.LOG_DIR / "test.log", "r") as file:
        message = file.read()

    os.remove(settings.LOG_DIR / "test.log")

    assert message == "Test message"
