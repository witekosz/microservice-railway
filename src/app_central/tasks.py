from __future__ import absolute_import

import time
from datetime import datetime

from celery.utils.log import get_task_logger
from celery import Celery

import settings
from utils import change_barrier_status, check_if_barrier_status_is_open, write_log

logger = get_task_logger(__name__)

app = Celery("tasks", broker=settings.DATABASE_URI, include=["tasks"],)


@app.task
def actual_speed(current_speed: int) -> int:
    """Checking train speed and appending to log file"""

    logger.info(f"Task {str(current_speed)}")
    now = datetime.now().strftime("[%H:%M:%S]")
    speed_level: str

    if 0.0 <= current_speed <= 40.0:
        speed_level = "slow"
    elif 40.1 <= current_speed <= 140.0:
        speed_level = "normal"
    elif 140.1 <= current_speed <= 180.0:
        speed_level = "fast"
    else:
        speed_level = "error"
        logger.error(f"{now}: {str(current_speed)}")

    # Write to log file
    write_log(f"{speed_level}.log", f"{now} : {str(current_speed)}\n")

    return current_speed


@app.task
def train_station(station_name: str) -> str:
    """Receiving station and checking barrier status"""
    logger.info(f"Task {station_name}")
    now = datetime.now().strftime("[%H:%M:%S]")

    write_log("info.log", f"{now} : {station_name}\n")

    barrier_status: bool = check_if_barrier_status_is_open()
    logger.info(f"Barrier status: {barrier_status}")

    if barrier_status:
        # Closing barrier
        change_barrier_status(False)
        logger.info(f"Closing barrier")

        # Opening barrier after 10 seconds
        time.sleep(10)
        time_opened = change_barrier_status(True)["time"]
        logger.info(f"Opening barrier")
        write_log("info.log", f"{time_opened} : barrier opened : {station_name}\n")

    else:
        logger.info(f"Anomaly: {station_name}")
        write_log("info.log", f"{now} : anomaly : {station_name}\n")

    return station_name
