import os
import pathlib


BASE_DIR = pathlib.Path(__file__).parent
LOG_DIR = BASE_DIR / "logs"

API_URL = os.environ.get("API_URL")
DATABASE_URI = os.environ.get("DATABASE_URI")
