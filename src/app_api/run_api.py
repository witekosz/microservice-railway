import click
from flask import Flask
from flask.cli import with_appcontext
from flask_restful import Api
from flask_sqlalchemy import SQLAlchemy

from .api import settings

__version__ = (1, 0, 0, "dev")

db = SQLAlchemy()


def create_app(test_config: dict = None):
    """Create and configure an instance of the Flask application."""
    app = Flask(__name__, instance_relative_config=True)
    api = Api(app)  # register flask_restful

    app.config.from_mapping(
        SECRET_KEY=settings.SECRET_KEY,
        SQLALCHEMY_DATABASE_URI=settings.FLASK_DATABASE_URI,
        SQLALCHEMY_TRACK_MODIFICATIONS=False,
        DEBUG=True,
    )
    # load the test config if passed in
    if test_config:
        app.config.update(test_config)

    # initialize Flask-SQLAlchemy and the init-db command
    db.init_app(app)
    app.cli.add_command(init_db_command)

    @app.route("/")
    def index():
        """Index view with helpful urls"""
        return "TRAIN API", 200

    # API ROUTES
    from .api import views

    api.add_resource(views.BarrierView, "/api/v1/barrier")

    return app


def init_db():
    db.drop_all()
    db.create_all()


@click.command("init-db")
@with_appcontext
def init_db_command():
    """Clear existing data and create new tables."""
    init_db()
    click.echo("Initialized the database.")
