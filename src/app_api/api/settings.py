import logging
import os


# Logging
LOGGER_FORMAT = "%(asctime)s %(message)s"

logging.basicConfig(format=LOGGER_FORMAT, datefmt="[%H:%M:%S]")
logger = logging.getLogger()
logger.setLevel(logging.INFO)

SECRET_KEY = os.environ.get("SECRET_KEY", "Dkw77N9L8dNJ")

# DB settings
FLASK_DATABASE_URI = os.environ.get("FLASK_DATABASE_URI")
