from sqlalchemy import inspect

from ..run_api import db


def as_dict(obj: object) -> dict:
    """Converting SQLAlchemy object to dict"""

    return {c.key: getattr(obj, c.key) for c in inspect(obj).mapper.column_attrs}


class BarrierStatus(db.Model):
    """Model for barrier status"""

    __tablename__ = "barrier_status"

    id = db.Column(db.Integer, primary_key=True)
    status = db.Column(db.String(80), nullable=False)
    time_stamp = db.Column(db.DateTime, nullable=False, server_default=db.func.now())

    def __repr__(self):
        return f"<BarrierStatus {self.status}: {self.time_stamp}>"
