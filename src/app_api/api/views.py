from flask import request
from flask_restful import Resource
from sqlalchemy import desc
from sqlalchemy.exc import ProgrammingError

from ..run_api import db
from .models import BarrierStatus, as_dict


class BarrierView(Resource):
    def get(self):
        """Read barrier status"""

        try:
            status_instance: object = BarrierStatus.query.order_by(
                desc("time_stamp")
            ).first()
        except ProgrammingError:

            return {"error": "Database not initiated"}, 404

        if status_instance:
            barrier_status: str = as_dict(status_instance)["status"]

            return {"status": barrier_status}, 200
        else:

            return {"error": "No data available"}, 404

    def post(self):
        """Change barrier status"""
        barrier_status: str = request.form["barrier_status"]

        if barrier_status in ["opened", "closed"]:
            status_instance: object = BarrierStatus(status=barrier_status)
            db.session.add(status_instance)
            db.session.commit()

            return {"status": barrier_status}, 200
        else:

            return {"error": "Invalid argument: (opened/closed)"}, 400
