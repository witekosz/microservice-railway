import pytest

from ..models import BarrierStatus
from ...run_api import create_app, db


@pytest.fixture
def app():
    """Create and configure a new app instance for each test."""
    # create the app with common test config
    app = create_app({"TESTING": True})

    # create the database and load test data
    with app.app_context():
        db.session.add(BarrierStatus(status="opened"))
        db.session.commit()
    yield app


@pytest.fixture
def client(app):
    """A test client for the app."""
    return app.test_client()
