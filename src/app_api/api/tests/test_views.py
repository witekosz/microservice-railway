from ..models import BarrierStatus


def test_index_view(client):
    response = client.get("/")

    assert response.status_code == 200
    assert "TRAIN API" in response.get_data(as_text=True)


def test_get_barrier(client):
    response = client.get("/api/v1/barrier")

    assert response.status_code == 200
    assert response.is_json
    assert response.get_json()["status"] in ["opened", "closed"]


def test_post_barrier_no_data(client):
    response = client.post("/api/v1/barrier")

    assert response.status_code == 400


def test_post_barrier_err_data_key(client):
    response = client.post("/api/v1/barrier", data={"barrier": "closed"})

    assert response.status_code == 400


def test_post_barrier_err_data(client):
    response = client.post("/api/v1/barrier", data={"barrier_status": "a"})

    assert response.get_json()["error"] == "Invalid argument: (opened/closed)"
    assert response.status_code == 400


def test_post_barrier_corr_data(client, app):
    response = client.post("/api/v1/barrier", data={"barrier_status": "opened"})

    with app.app_context():
        assert BarrierStatus.query.first() is not None

    assert response.get_json()["status"] in ["opened", "closed"]
    assert response.status_code == 200
