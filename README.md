# Microservice Railway

## About

### General

- **Python 3.7** with static typing
- code formatting - **Black** (from IDE)
- testing - **pytest**

### Service I API

Available endpoints: 
- **api/v1/barrier**
    * GET - Check current barrier status
    * POST - Change current barrier status, required form data `barrier_status`
    param(_opened_/_closed_)

##### Tools
- web framework - **Flask**, **Flask-RESTful**
- database - **PostgreSQL 11**
- db connection - **psycopg2**, **SQLAlchemy ORM**

### Service II Central
Managing two task:

1. **actual_speed** - after receiving saves time and value in _slow.log_, _normal.log_ or _fast_speed.log_ 
depending on received value
2. **train_station** - after receiving check status of barrier from API service.
 If barrier is opened sends request to close it down, then after 10 seconds send request to open it
 and saves time opened in in _info.log_ file. If barrier is closed saves value as anomaly in _info.log_ file.
##### Tools
- task manager - **celery**

### Service III Train

Simulates train movement.
Send every 10 seconds task with speed and very 180 seconds task with current station to central app.

##### Tools
- task manager - **celery**


### Running local

Run app by docker compose locally(on Linux):
1. Open terminal
2. Copy git repository
3. Go to the project directory:
    ```sh
    $ cd microservice_railway
    ```
4. Build images:
    ```sh 
   $ docker-compose build
   ```
5. Run containers:
    ```sh
    $ docker-compose up -d
    ```
6. Create a database:
    ```sh
    $ docker-compose exec app_api flask init-db
    ```
7. Run tests:
    ```sh
    $ docker-compose exec app_api python -m pytest
    ```
   
   ```sh
    $ docker-compose exec app_train python -m pytest
    ```
   
   ```sh
   $ docker-compose exec app_central python -m pytest
    ```

8. Scaling train service by n number:
    ```sh
    $ docker-compose up -d --scale app_train=n
    ```

8. The API interface is running on: [localhost:5000/api/v1/barrier](http://localhost:5000/api/v1/barrier)
